## Théorie

Ces deux termes se référents à la facon dont on update ou rollback un [[Deployment]] pour nos applications.

Quand on crée un [[Deployment]], un rollout est déclenché. Un nouveau rollout crée une nouvelle révision d'un [[Deployment]].

Il y a deux stratégies de [[Deployment]].

1. Détruire tous les [[Pod]]s de l'ancienne version et ensuite créer les nouveaux. Le problème de cette stratégie est que durant la période où les anciens [[Pod]]s sont down et les nouveaux ne sont pas encore up, l'application est inaccessible. Cette stratégie est connue sous le nom de *Recreate*. Ce n'est pas la stratégie par défaut.
2. La seconde stratégie consiste à détruire **UN** seul vieu [[Pod]] et ensuite créer un nouveau. Et ainsi de suite jusqu'à ce que tous les anciens soient remplacés par des nouveaux. Il n'y a pas de downtime de l'application. Il s'agit de la stratégie *Rolling Update* et est celle par défaut.

Quand on upgrade notre application avec un nouveau [[Deployment]], Kubernetes va créer un nouveau [[ReplicaSets]] et ensuite créer les nouveaux [[Pod]]s qui seront gérés par le [[ReplicaSets]].

Si lors de l'upgrade de l'application il apparaît que la nouvelle version est problématique, il est possible de  facilement effectuer un rollback vers la version antérieure. En effectuant un rollout, Kubernetes va détruire les [[Pod]]s créés dans le nouveau [[ReplicaSets]] et créer des [[Pod]]s dans le [[ReplicaSets]] précédent, restaurant ainsi l'application à sa version antérieure.
## Commandes

`kubectl rollout status <deployment name>`
Permet de voir le status d'un [[Deployment]]

`kubectl rollout history <deployment name>`
Permet de voir les révisions et l'historique d'un [[Deployment]].

`kubectl apply -f <deployment file>`
Permet d'appliquer une nouvelle version d'un [[Deployment]].

`kubectl set image <deployment name> <container name>=<image name>`
Permet de mettre à jour l'image d'un container dans un [[Deployment]].

`kubectl rollout undo <deployment name>`
Permet de détruire les [[Pod]]s dans le nouveau [[ReplicaSets]] et remonter les anciens [[Pod]]s du vieux [[ReplicaSets]] précédent.