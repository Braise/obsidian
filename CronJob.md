[Documentation k8s](https://kubernetes.io/docs/concepts/workloads/controllers/cron-jobs/)

## Théorie

Un CronJob est un [[Job]] qui peut être schedule. Le format pour spécifier la périodicité est Cron format, comme sur un système Unix :

![[cronformat.png]]

## Exemple:

```
apiVersion: batch/v1
kind: CronJob
metadata:
  name: hello
spec:
  schedule: "* * * * *"
  jobTemplate:
    spec:
      template:
        spec:
          containers:
          - name: hello
            image: busybox:1.28
            imagePullPolicy: IfNotPresent
            command:
            - /bin/sh
            - -c
            - date; echo Hello from the Kubernetes cluster
          restartPolicy: OnFailure
```