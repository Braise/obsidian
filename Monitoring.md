## Théorie

Il faut choisir ce que l'on veut monitorer. Il est possible de monitorer tant les Nodes, que les [[Pod]], que les [[Service]], que le trafic réseau, etc...

On peut par exemple monitorer le nombre de Nodes, le CPU et la mémoire utilisés par celles-ci, le trafic réseau sur les Nodes etc...
Et toute ces métriques peuvent également l'être au niveau des [[Pod]].

On a donc besoin d'une solution pour collecter ces données, les stockées et nous permettres de les exploiter selon nos besoins.

Il existe plusieurs solutions répondant à ce besoin :
- Metrics Server
- Prometheus
- Elastic Stack
- DataDog (propriétaire)
- Dynatrace (propriétaire)

Sur chaque Node, un agent est déployé. Il s'agit du *Kubelet*. Son rôle est de recevoir des instructions de l'API de Kubernetes et runner les [[Pod]]s sur la Node Le *Kubelet* contient un sous-composant, le *cAdvisor*, dont la tâche est de récupérer les métriques de performances des [[Pod]]s afin de les exposer au travers de l'API du *Kubelet*, les rendants ainsi disponibles à la solution de monitoring.

## Commandes

`kubectl top node`
Montre l'usage des ressources des Nodes.

`kubectl top pod`
Montre l'usage des ressources des [[Pod]]s.