[K8S Documentation](https://kubernetes.io/docs/reference/access-authn-authz/authentication/)

## Théorie

K8S ne gère pas les user accounts. On ne peux pas créer de user dans un cluster. Il faut se servir d'un mécanisme externe pour cela, par exemple LDAP.

K8S gère par contre les [[Service Account]]s.

K8S gère aussi les accès des users, que ce soit via `kubectl` où via l'api directement. Peut importe le moyen utilisé, les requests passent obligatoirement par le *kube-apiserver*. ce dernier commence par authentifier l'utilisateur avant de process la request :

![[kube-apiserver.png]]

Pour réaliser l'authentification, différents mécanismes peuvent être configurés :

* Utiliser des certificats
* Utiliser un service d'authentification (LDAP par exemple)

### Une liste de mot de passes dans un fichier 

On peut créer un fichier csv avec les utilisateurs et leur mot de passes et utiliser cela. Le fichier doit contenir trois colonnes : password, username et user id. On passe ensuite le fichier comme une option au *kube-apiserver* :

![[kube-apiserver_basic-auth-file.png]]

Une quatrième colonne peut être ajoutée avec des groupes pour les users.

### Une liste de token dans un fichier

Similaire au fichier avec les utilisateur :

![[kube-apiserver_token_file.png]]





