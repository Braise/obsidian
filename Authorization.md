[Documentation k8s](https://kubernetes.io/docs/reference/access-authn-authz/authorization/)

## Théorie

Une fois que quelqu'un a obtenus accès au cluster à travers le mécanisme d'[[Authorization]], l'authorization permet de définir ce qu'il peut faire.

Il y a différents mécanisme d'authorization supporté par K8s :

### Node

### Attribute Based Authorization

On associe un utilisateur ou un groupe d'utilisateurs à un ensemble de permission.

### Role Based Authorization

Au lieu d'associer un utilisateur ou groupe d'utilisateurs à un ensemble de permession, on va définir un rôle pour cet ensemble de permission. Ensuite il suffit simplement d'associer des utilisateurs au rôle créé.

Il suffit donc de créer un rôle :

```yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: default
  name: developer
rules:
- apiGroups: [""] # "" indicates the core API group
  resources: ["pods"]
  verbs: ["get", "watch", "list"]
- apiGroups: [""] # "" indicates the core API group
  resources: ["ConfigMap"]
  verbs: ["get", "watch", "list"]
```

Ensuite il faut créer le lien entre un utilisateur et un rôle :

```
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
	name: devuser-developer-binding
subjects: # Section où l'on précise les détails de l'utilisateur
	- kind: User
	  name: dev-user
	  apiGroup: rbac.authorization.k8s.io
roleRef: # Section où l'on précise les détails du rôle créé précédemment
	kind: Role
	name: developer
	apiGroup: rbac.authorization.k8s.io
```

### Webhook

Si on veut gérer les authorizations par un un service tiers et ne pas utiliser les mécanisme built-in de K8S.

## Commandes

`kubectl auth can-i create deployments`
Permet de valider si l'on peut faire quelque chose. Par exemple ici créer un deployement.

`kubectl auth can-i create deployments --as <utilisateur>`
Permet de valider si l'on peut faire quelque chose en impersonnant un utilisateur. Seulement accessible pour les administrateurs.

`kubectl describe pod kube-apiserver-controlplane -n kube-system`
Obtenir les informations du kube-apiserver.

`kubectl get role --all-namespaces`
Obtenir les rôles de tous les namespaces.