[Documentation k8s](https://kubernetes.io/docs/concepts/services-networking/ingress)

## Théorie

Un objet de Kubernetes en charge de gérer le trafic *externe* vers un [[Service]] dans le cluster, généralement via http et https :

![[Ingress.png]]

En plus de rendre un [[Service]] accessible, Ingress joue également un rôle de load balancer, gère TLS et SSL et fournit un name-based virtual hosting.

Ingress est constitué de deux grandes partie :

### 1. Ingress Resources

Un ensemble de règles et de configurations qui vont être utilisées par un Ingress Controller. Ces règles permettent de router le trafic entrant vers le [[Service]] approprié :

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: minimal-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
spec:
  ingressClassName: nginx-example
  rules:
  - http:
      paths:
      - path: /testpath
        pathType: Prefix
        backend:
          service:
            name: test
            port:
              number: 80
```

### 2. Ingress Controller

Un Ingress Controller fait office de load balancer et de reverse proxy. Il accepte le trafic de l'extérieur et le dirige vers les [[Service]]s adéquats. Pour ce faire il converti les Ingress Resources en routing rules.

**Attention !!!!**
Contrairement à la vaste majorité des objets, un Ingress controller n'est *pas* démarré automatiquement avec le Cluster. Il faut le démarrer à la main en ayant au préalable choisis une implémentation, où créé une. Les implémentations les plus courantes sont Nhinx, HaProxy, traefik, Contour, Istio.
Une fois qu'un controller est en place il est valable pour tout le cluster.