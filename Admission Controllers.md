[Documentation k8s](https://kubernetes.io/docs/reference/access-authn-authz/admission-controllers/)

## Théorie

Permet de mieux controller comment un cluster est utilisé en ajoutant d'autre règles de sécurité. Par exmple on pourrait interdire l'utilisation d'image venant d'un repo public, obligé l'utilisation de certains metadata, etc...

Dans le pipeline d'exécution d'une commande, l'admission controller vient juste après l'[[Authentication]] et l'[[Authorization]] :

![[pipeline_commande_kubectl.png]]

En plus de valider la configuration, le controller peut également modifier celle-ci ou effectuer d'autres opérations avant que la commande soit effectivement roulée.

Il y a plusieurs controller built-in dans K8s.

Les controllers peuvent être divisés en deux grandes catégories, et certains peuvent faire partie des deux catégories :

### Validating Admission Controllers
Valide qu'une certaine configuration est bien appliquée.

### Mutating Admission Controllers
Modifie l'objet avant qu'il soit créé.

Généralement les Mutating sont invoqués avant les Validating, afin de s'assurer que l'objet final qui va être créé est correct.

### Créer ses propres controllers
Pour créer des controllers, il faut intéragir avec deux webhook : *MutatingAdmissionWebhook* et *ValidatingAdmissionWebhook* .

Ces webhooks peuvent être configurés pour pointer sur un serveur qui est hébergé dans le cluster ou à l'extérieur.

Quand les webhooks sont invoqué dans le pipeline, un appel est fait au serveur avec un objet json représentant une *AdmissionReview* :

![[WebhookServer.png]]

Le webhook  serveur peut être une API utilisant n'importe quel langage (Go, ASP....). La seule contrainte est qu'il doit accepter l'objet envoyé par K8s et produire une réponse correspondant au format attendu par K8s.

Une fois le serveur déployé, il faut configurer le cluster pour intéragir avec à travers un *ValidatingWebhookConfiguration* objet :

![[ValidatingWebhookConfiguration.png]]

Ici un exemple avec un serveur configuré dans le cluster.

## Commandes

`kube-apiserver --eanble-admission-plugins=<liste séparée par des virgules>`
Active un ou plusieurs admission controllers

`kube-apiserver --disable-admission-plugins=<liste séparée par des virgules>`
Désactive un ou plusieurs admission controllers

### IMPORTANT
les commandes `kube-apiserver` doivent être roulées dans le pod `kube-apiserver-controlplane`. 
On peut faire ca en utilisant `kubectl exec -it <nom du pod> -n <namespace du pod> -- <commande à rouler>`.
