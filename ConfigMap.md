[Documentation k8s](https://kubernetes.io/docs/concepts/configuration/configmap/)

## Théorie

Objet utilisé pour stocker des données **non confidentielles** sous forme de dictionnaire.
Les [[Pod]] peuvent utiliser des ConfigMap de différentes facon :

- Variables d'environnement
- Argument de lignes de commande
- Fichier de configuration dans un [[Volume]].

Les [[Pod]] utilisant un ConfigMap sous forme de [[Volume]] voient les valeurs de celui-ci update automatiquement quand elles le sont dans le ConfigMap monté. Ce n'est pas le cas pour les [[Pod]]
utilisant les variables d'environnement.

Il est recommendé d'utiliser des ConfigMap pour gérer des informations de configuration séparément du code de l'application.

Un ConfigMap ne devrait jamais être utilisé pour stocker de grande quantité de données.
## Exemple

```
apiVersion: v1
kind: ConfigMap
metadata:
  name: game-demo
data:
  # property-like keys; each key maps to a simple value
  player_initial_lives: "3"
  ui_properties_file_name: "user-interface.properties"

  # file-like keys
  game.properties: |
    enemy.types=aliens,monsters
    player.maximum-lives=5    
  user-interface.properties: |
    color.good=purple
    color.bad=yellow
    allow.textmode=true    
```

Et voici un exemple d'un [[Pod]] consommant le ConfigMap sous forme de variable d'environnement :
```
apiVersion: v1
kind: Pod
metadata:
  name: configmap-demo-pod
spec:
  containers:
    - name: demo
      image: alpine
      command: ["sleep", "3600"]
      env:
        # Define the environment variable
        - name: PLAYER_INITIAL_LIVES # Notice that the case is different here
                                     # from the key name in the ConfigMap.
          valueFrom:
            configMapKeyRef:
              name: game-demo           # The ConfigMap this value comes from.
              key: player_initial_lives # The key to fetch.
        - name: UI_PROPERTIES_FILE_NAME
          valueFrom:
            configMapKeyRef:
              name: game-demo
              key: ui_properties_file_name
```

Et voici un [[Pod]] consommant un ConfigMap sous forme de [[Volume]] :

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
  - name: mypod
    image: redis
    volumeMounts:
    - name: foo
      mountPath: "/etc/foo"
      readOnly: true
  volumes:
  - name: foo
    configMap:
      name: myconfigmap
```

## Commandes

`kubectl get configmap`
Liste les ConfigMap.

`kubectl describe configmap <name>`
Décrit un ConfigMap.

`kubectl create configmap --from-literal=<cle>=<valeur> --from-literal=<cle>=<valeur> <name>`
Crée un ConfigMap.