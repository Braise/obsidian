[Documentation k8s](https://kubernetes.io/docs/concepts/services-networking/network-policies/)

## Théorie

Permet de contrôler le trafic réseau de et vers un [[Pod]]. On y définis comment un [[Pod]] peut communiquer avec d'autres entitées de Kubernetes.

Par défaut, Kubernetes autorise le trafic de tous les [[Pod]]s vers toutes les destinations.

Il existe deux grands type de trafic :

1. Ingress : Signifie le trafic qui va du [[Pod]] vers une autre entitée
2. Egress : Signifie le trafic qui arrive dans le [[Pod]]

On définit une network policy en commencant par spécifier sur quel [[Pod]] elle va s'appliquer un utilisant un selecteur.
Ensuit on précise pour quel type de trafic elle s'applique.
Une fois cela fais, on spécifie la règle, c'est-à-dire que l'on précise quel(s) [[Pod]](s) sont autorisés et sur quels ports.

## Exemple

```
apiveVersion: networking.k8s.io/v1
kind: NetworkPolicy
metadata:
	name: db-policy
spec:
	podSelector:
		matchLables:
			role: db
	policyType:
	- Ingress
	ingress:
	- from:
		- podSelector:
			matchLabels:
				name: api-pod
		ports:
			- protocol: TCP
			  port: 3306
```

Ici une network policy s'appliquant à tous les [[Pod]]s ayant le label `role` avec la valeur `db` qui est de type Ingress. Le trafic entrant ne sera accepté que pour les [[Pod]]s ayant le label `name` avec la valeur `api-pod` sur le port 3306 et pour le protocol TCP.

**Attention !!!**  
Le [[Pod]] selector fonctionne pour n'importe quel [[Pod]] de n'importe quel namespace! Si l'on veut également une restriction sur le namespace, il faut ajouter la propriété `namespaceSelector`.
De même, si on omet le `podSelector` et que l'on ajuste un selecteur sur le namespace, cela veut dire que tous les [[Pod]]s du namespace sélectionné peuvent accéder à l'entité visée par la network policy.

Il est également possible d'ajouter une propriété `ipBlock` permettant de spécifier un range ayant accès au [[Pod]](s) sur visé(s) par la network policy.

## Commandes

`kubectl get networkpolicies`
Liste les network policies.