[Documentation k8s](https://kubernetes.io/docs/concepts/workloads/pods/)

## Théorie

Plus petite unité possible que l'on peut créer et gérer dans Kubernetes. Il s'agit d'un groupe de **un ou plusieurs** container.
Un application roulera donc toujours dans un Pod.

On peut voir le Pod comme un environnement partagé et les containers comme des applications processes.

Il est **très** important d'avoir conscience qu'un Pod est éphémère et peut être détruit et recréé.

### Ajout au container

L'utilisation d'un Pod permet d'ajouter les features suivantes à un container :

1. Labels et annotation
2. Restart policies
3. Probes (startup, readiness, liveness...)
4. Affinity et anti-affinity rules
5. Termination control
6. Security polies
7. Resource requests and limits

### Lifecycle

un Pod dispose d'un status et de conditions. Le status nous indique où le Pod en est dans son cycle de vie. Quand il est créé il est en *pending state*, cela signifie que le Scheduler est occupé à déterminer où placer le Pod. Tant que le Scheduler ne peut trouver une Node où placer le Pod, ce dernier reste dans cet état.

Quand le Scheduler a trouvé une Node, le Pod entre dans l'état *container creating*. A ce stade, les images sont pull et les containers sont démarrés.

Une fois que les containers ont levé, le Pod est dans l'état *running*. Il reste dans cet état jusqu'à ce que le program du container soit terminé où qu'il soit kill.

Les conditions sont un complément d'information au status d'un Pod. Il s'agit d'un tableau de valeurs vraies où false nous donnant plus d'information:

|Condition|Description|
|-----------|-------------|
|PodSchedule| Indique si un Pod est schedule pour déploiement sur une Node |
|Initialized| Le Pod est initialisé|
|ContainersReady| Tous les containers dans le Pod sont prêt|
|Ready| Quand les trois conditions au dessus sont au vert|

### Scheduling

Un Pod garantit que chaque container au sein de ce dernier sera assigné à la même worker node. 
L'utilisation des labels, des affinity rules et des resources requests permet de gérer également sur quels worker node un Pod peut rouler.

### Partage de ressources

Les containers au sein d'un Pod partagent les ressources suivantes :

- Filesystem
- Network stack (IP Address, routing table, ports...)
- Mémoire
- Volumes

## Exemple
``
```
apiVersion: v1
kind: Pod
metadata:
  name: nginx
spec:
  containers:
  - name: nginx
    image: nginx:1.14.2
    ports:
    - containerPort: 80
```
``
![[Structure_pod.png]]

## Multi-Container Pods

Comme mentionné précédemment, un Pod peut contenir un où plusieurs container. Si un Pod contient plus de un containers, alors chaque container suit le même cycle de vie. C'est-à-dire qu'ils sont créés et détruits ensemble. Ils partagent le même network space et ont accès au même storage volumes.

Il y a trois principaux patterns pour les Multi-Container Pods :

### Sidecar
[Documentation](https://progressivecoder.com/understanding-sidecar-design-pattern-with-kubernetes-pod/)

Le pod contient deux containers : l'Application container et le Sidecar container. 

L'Application container contient la logique principale de l'application et le Sidecar container vient augmenter et améliorer cette logique. De plus, le container de l'application n'est pas conscient de cette augmentation et amélioration. 

On utilise donc le Sidecar pour ajouter des fonctionnalités sans faire de gros changements à l'Application container.

Par exemple, on pourrait utiliser en Sidecar un logging agent dont le rôle serait de collecter les logs du server web (l'Application container) et de les envoyer à un serveur de log.

### Adapter
[Documentation]([Kubernetes Patterns: The Adapter Pattern (weave.works)](https://www.weave.works/blog/kubernetes-patterns-the-adapter-pattern))

Très proche du Sidecar container, l'Adapter pattern se charge de présenter une interface compatible avec d'autre système.

Par exemple, un Adapter peut lire les logs d'une application et les formatter en une version compréhensible par la solution de monitoring (Prometheus par exemple).

Contrairement au Sidecar pattern, l'Adapter ne vient pas ajouter des fonctionnalités à l'application de base, il se charge simplement d'adapter tout où partie de celle-ci pour la rendre intelligible pour certains clients.

### Ambassord
[Documentation]([Kubernetes — Learn Ambassador Container Pattern | by Bhargav Bachina | Bachina Labs | Medium](https://medium.com/bb-tutorials-and-thoughts/kubernetes-learn-ambassador-container-pattern-bc2e1331bd3a))

Le rôle de ce container est de fournir un accès à des services externes à l'application. 
On utilise ce pattern lorsque l'on ne veut pas gérer dans l'application principale la complexité d'accès à ces services et on la délègue à un Ambassador qui a la charge de réaliser les appels et de les formaters en quelque chose de compréhensible par le container de l'application.

## Init Containers
Dans un contexte de pods multi containers, on s'attend à ce que chaque container roule durant toute la durée de vie du pod.

Cependant, il existe un cas particulier où l'on voudrait que le processus d'un container se termine alors que le pod est toujours en activité. Ce sont les Init Containers.

Il sont généralement utilisés pour setup l'application principale et une fois ce job fait, il se termine et donne la main au container primaire du pod. Voici le yaml d'un pod avec un Init container :

```
apiVersion: v1
kind: Pod
metadata:
  name: myapp-pod
  labels:
    app: myapp
spec:
  containers:
  - name: myapp-container
    image: busybox:1.28
    command: ['sh', '-c', 'echo The app is running! && sleep 3600']
  initContainers: ## <= Ici la section qui speçifie l'Init container
  - name: init-myservice
    image: busybox
    command: ['sh', '-c', 'git clone <some-repository-that-will-be-used-by-application> ;']
```


On peut configurer plusieurs Init container. Ils rouleront chacun à leur tour et dans l'ordre dans lequel ils sont déclarés.

## Commandes

`kubectl get pods`
Permet de lister les Pods .
Dans le résultat de cette commande, la commande ready indique le nombre de container running sur le nombre de container total :

![[pods_ready.png]]

On voit ici par exemple que le Pod webapp a seulement un seul container running sur les deux qui sont censés tourner dans le Pod.

`kubectl run --image=nginx nginxpod` 
crée un nouveau Pod basé sur l'image nginx et nommé nginxpod

`kubectl describe pod <name>`
permet de voir le détail d'un Pod en spécifiant son nom

`kubectl delete pod <name>`
Permet de delete un Pod

`kubectl run --image=redis123 --dry-run=client -o yaml redis > redis-definition.yaml` 
Permet de créer un manifest file au format yaml pour un Pod utilisant l'image redis123

`kubectl edit pod <name>`
Permet d'éditer un Pod

`kubectl delete pod <name>`
Permet de delete un Pod

`kubectl exec -it <pod name> <commande> <argument commande>`
Roule une commande dans un Pod

`kubectl logs -f <pode name>`
Montre les logs du pods live. Si plusieurs containers roulent dans le Pod, il faut aussi spécifier le nom du container.