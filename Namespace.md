[Documentation k8s](https://kubernetes.io/docs/concepts/overview/working-with-objects/namespaces/)

## Théorie

Mécanisme permettant d'isoler des groupes de ressources au sein d'un cluster. Au sein d'un Namespace, les noms de ces ressources doivent être uniques.

On retrouve dans un Namespace des ressources comme des :
- [[Service]]
- [[Pod]], 
- [[ReplicaSets]]
- [[Secret]]
- [[ConfigMap]]
- [[Deployment]]
- [[Volume]]

### DNS

Quand un crée un [[Service]], une entrée DNS est créée pour ce dernier. Cette entrée respecte la forme `<service-name>.<namespace-name>.svc.cluster.local`. Cela signifie que si un container utiliser juste `<service-name>` cela resoudra le [[Service]] local au Namespace. 
Pour résoudre un [[Service]] dans un Namespace différent, il faut utiliser le nom complet.

## Commandes

`kubectl get namespace`
Liste les Namespace.

`kubectl get pods --namespace=research`
Liste les Pod du Namespace research.

`kubectl get pods --all-namespaces`
Liste les Pod de tous les Namespace

`kubectl config set-context --current --namespace=<insert-namespace-name-here>`
Configure le namespace courant