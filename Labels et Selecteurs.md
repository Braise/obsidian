[Documentation k8s](https://kubernetes.io/docs/concepts/overview/working-with-objects/labels/)

## Théorie

Méthode standard pour grouper et filtrer des éléments.

Un label est une propriété attachée à un où plusieurs éléments :

![[Labels.png]]

Un sélecteur permet de filtrer les éléments :

![[Selecteurs.png]]

Kubernetes permet de grouper et sélectionner des objets ([[Pod]], [[Deployment]], [[ReplicaSets]], [[Service]], [[ConfigMap]] etc..) en se basant sur les labels et les sélecteurs.

Kubernetes utilise les labels et les sélecteurs pour connecter des objets ensembles. Par exemple les [[ReplicaSets]] monitorent les [[Pod]]s dont ils ont la responsabilité de cette facon :

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
	name: simple-webapp
	labels:
		app: App1
		function: Front-end
spec:
	replicas: 3
	selector: # => ici on défini le sélecteur permettant de grouper les pods
		matchLables:
			app: App1
	template:
		metadata:
			labels:
				app: App1 # => Le label du Pod correspond au selector du ReplicaSet
				function: Front-end
		spec:
			containers:
			- name: simple-webapp
			  image: simple-webapp
```

Les [[Service]]s fonctionnent de la même facon pour trouver les [[Pod]]s auxquels envoyer le trafic.

## Commandes

`kubectl get pods --selector app=App1`
Obtient tous les [[Pod]]s ayant le label app avec la valeur App1