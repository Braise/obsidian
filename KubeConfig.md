[Documentation K8S](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/)

## Théorie

Fichier de configuration. Un fichier de configuration est utiliser par défaut à l'emplacement suivant : `$HOME/.kube/config` Si on crée le fichier à cet endroit, c'est la configuration contenue dans ce dernier qui sera utilisé par défaut avec `kubectl`.

Le fichier est divisé en trois sections  : Clusters, Contexts et Users.

### Clusters

Les différents Clusters auxquels on doit avoir accès.

### Users

Les utilisateurs qui ont accès aux Clusters.

### Contexts :

Établit un lien entre Clusters et Users qui permet de définir quels users ont accès à quels Clusters.

![[kubeConfig_sections.png]]

## Exemple

![[kubeConfig_exemple.png]]