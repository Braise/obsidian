[Documentation k8s](https://kubernetes.io/docs/concepts/storage/volumes/)
## Théorie

Un container, et donc un [[Pod]] par extension, est transient par définition. Une fois la tâche demandée terminée, le container est détruit. Cela rend la persistence des données compliquée.

Pour palier à ce problème est conserver les données voulues après la destruction d'un container, on y attache un volume lors de la création.

Voici un exemple de volume utilisant un dossier sur le host :

```
apiVersion: v1
kind: Pod
metadata:
  name: test-pd
spec:
  containers:
  - image: registry.k8s.io/test-webserver
    name: test-container
    volumeMounts:
    - mountPath: /test-pd
      name: test-volume
  volumes:
  - name: test-volume
    hostPath:
      # directory location on host
      path: /data
      # this field is optional
      type: Directory
```

Dans un environnement avec beaucoup d'applications, on va préférer utiliser des *Persistant Volumes* plutôt que de laisser les utilisateurs définir leurs propres de leur bord. Il s'agit d'un cluster wide pool de storage volume configurés par un administrateur :

![[PersistentVolumes.png]]

Voici un exemple de Persistent Volume :

```
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pv0003
spec:
  capacity:
    storage: 5Gi
  volumeMode: Filesystem
  accessModes: # Définis comment le volume doit être monté dans le Pod
    - ReadWriteOnce
  persistentVolumeReclaimPolicy: Recycle
  storageClassName: slow
  mountOptions:
    - hard
    - nfsvers=4.1
  nfs:
    path: /tmp
    server: 172.17.0.2
```

Pour accéder ce pool, il est nécessaire de disposer d'un *Persistent Volume Claim*. Ce claim est créé par l'utilisateur qui veut utiliser un *Persistent Volume*. Une fois qu'un claim est créé, K8s va le bind à un *Persistent Volume* en se basant sur les requests et propriétés du volume.

Un claim est attaché à uniquement un seul volume!

Si plusieurs volumes sont éligibles pour le claim, il est possible de préciser le volume désirer au besoin en utilisant des [[Labels et Selecteurs]].

Il y a une relation un-a-un entre un claim et un volume. Une fois qu'un claim est attaché à un volume, aucun autre claim ne peut s'attacher au même volume!

Si un claim ne peut être attribué à un volume, il restera en *pending* jusqu'à ce qu'un nouveau volume satisfaisant les conditions soit disponible.

Voici un exemple de *Persistant Volume Claim* :

```
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: myclaim
spec:
  accessModes:
    - ReadWriteOnce
  volumeMode: Filesystem
  resources:
    requests:
      storage: 8Gi
  storageClassName: slow
  selector:
    matchLabels:
      release: "stable"
    matchExpressions:
      - {key: environment, operator: In, values: [dev]}
``` 

Et voici comment l'utiliser dans un [[Pod]] :

```
apiVersion: v1
kind: Pod
metadata:
  name: mypod
spec:
  containers:
    - name: myfrontend
      image: nginx
      volumeMounts:
      - mountPath: "/var/www/html"
        name: mypd
  volumes:
    - name: mypd
      persistentVolumeClaim:
        claimName: myclaim
```

Quand un claim est détruit, on peut choisir ce qui arrive au volume utilisé. Par défaut ce dernier est *retain*, c'est-à-dire qu'il est ocnservé jusqu'à ce que l'administrateur le détruise à la main. Il est possible de le delete lorsque le claim est delete où de le recycler en supprimant les données contenues et rendant le volume à nouveau disponible pour d'autres claims.