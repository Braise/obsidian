[Documentation k8s](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/)

## Théorie

Un Deployment décrit un état désiré que le Deployment Controller va s'assurer d'atteindre et de maintenir.

Un Deployment va donc créer un [[ReplicaSets]] qui va à son tour créer des [[Pod]].

Si jamais un Deployment est update, un rollout sera trigger **si et seulement si** un changement est appliqué au [[Pod]] template. Par exemple via les labels ou les images.

Un Deployment ne gère que un [[Pod]] template. Une application avec un front-end et différents services back-end devra avoir différents [[Pod]] template donc différents Deployment.

## Exemple

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx
  template:
    metadata:
      labels:
        app: nginx
    spec:
      containers:
      - name: nginx
        image: nginx:1.14.2
        ports:
        - containerPort: 80
```

![[Structure deployment.png]]

## Commandes

`kubectl get deployment`
Liste les Deployments.

`kubectl create deployment --image=httpd:2.4-alpine -o yaml <name>  > <name>.yaml`
Crée un fichier manifeste pour un Deployment utilisant l'image httpd:2.4-alpine 