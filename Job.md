[Documentation k8s](https://kubernetes.io/docs/concepts/workloads/controllers/job/)

## Théorie

Il n'y a pas que des applications à la durée de vie indéterminée (web server, database...) qui peuvent être mise en container et donc dans un [[Pod]]. Des applications qui effectuent certaines opérations et qui ensuite se terminent peuvent être mise en container.

Si une de ces applications est déployée dans un [[Pod]], lorsque le container s'arrête alors le [[Pod]] s'arrête aussi. Le comportement par défaut de Kubernetes et alors de restarter le container en boucle. Ce comportement est géré par la propriété `restartPolicy` dont la valeur est par défaut `always`.

Un Job permet de palier ce problème et de ne pas jouer avec la `restartPolicy`. Il s'agit de l'équivalent d'un [[ReplicaSets]] qui va gérer un certain nombre de [[Pod]] jusqu'à ce qu'une certaine tâche soit complétée. Si un des [[Pod]]s se termine en erreur, Kubernetes va recréer automatiquement un nouveau [[Pod]] jusqu'à ce que le nombre spécifié dans la propriété `completion` soit atteint.

Par défaut Kubernetes crée les [[Pod]]s séquentiellement, mais il est possible d'ajuster cela avec la propriété `parallelism`.

## Exemple

```
apiVersion: batch/v1
kind: Job
metadata:
  name: pi
spec:
  template:
    spec:
      containers:
      - name: pi
        image: perl:5.34.0
        command: ["perl",  "-Mbignum=bpi", "-wle", "print bpi(2000)"]
      restartPolicy: Never
  backoffLimit: 4
```

## Commandes

`kubectl get jobs`
Permet de voir les jobs.