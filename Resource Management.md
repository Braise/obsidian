[Documentation k8s](https://kubernetes.io/docs/concepts/configuration/manage-resources-containers/)

## Théorie
Quand on définis un [[Pod]] on peut spécifier quelles sont les ressources dont chaque container a besoin. Les ressources les plus fréquentes sont le CPU et la RAM mais il peut y en avoir d'autres.

Pour une ressource on peut spécifier une *Request*, c'est-à-dire le minimum dont le [[Pod]] a besoin pour rouler. On peut également spécifier une *Limite*, c'est-à-dire le maximum de la ressource que le [[Pod]] peut consommer.

Ainsi, un [[Pod]] peut consommer plus que ce qui est spécifié par la *Request* mais ne dépassera **jamais** la *limite*.

## Exemple

```
apiVersion: v1
kind: Pod
metadata:
  name: frontend
spec:
  containers:
  - name: app
    image: images.my-company.example/app:v4
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
  - name: log-aggregator
    image: images.my-company.example/log-aggregator:v6
    resources:
      requests:
        memory: "64Mi"
        cpu: "250m"
      limits:
        memory: "128Mi"
        cpu: "500m"
```