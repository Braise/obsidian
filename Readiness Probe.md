[Documentation k8s](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/)

## Théorie

Un [[Pod]] est ready quand l'application roulant à l'intérieure est prête et peut recevoir du trafic.

Mais que veut dire être ready?

Un script peut être prêt en quelque millisecondes, une BD en quelque seconde et un web server peut prendre plusieurs minute avant d'être prêt.
Une application peut aussi être prête, mais pas encore en état de recevoir du trafic. 

Il est important de définir correctement quand une application est ready car Kubernetes se base sur cette indication pour déterminer si le [[Service]] peut router du trafic vers le [[Pod]]. Par défaut, Kubernets assume que dès qu'un container est créé, le [[Pod]] est prêt à recevoir du trafic. 
C'est pour cette raison qu'il est important de définir correctement la readiness d'un [[Pod]], afin d'empêcher le service de diriger du trafic vers un [[Pod]] qui n'est pas prêt et de générer des erreurs pour les clients.

Il est donc possible pour un développeur de définir quand son application est ready au moyen de Readiness Probes. Par exemple, pour une API cela pourrait être via un endpoint, une probe pourrait valider si une BD est prête en testant un socket TCP...

Pour définir une Readiness Probe, il suffit d'ajouter le champs `readinessProbe` à la définition du [[Pod]] et de configurer le test. Par exemple ici pour un appel http :

```
apiVersion: v1
kind: Pod
metadata:
name: simple-webapp
labels:
	name: simple-webapp
spec:
	containers:
	- name: simple-webapp
	  image: simple-webapp
	  ports:
		  - containerPort: 8080
	  readinessProbe:
		  httpGet:
			  path: /api/ready
			  port: 8080
```

Il est possible de configurer la Readiness Probe pour un appel http, comme au dessu, mais également pour un appel tcp ou une commande à exécuter:

```
readinessProbe:
	tcpSocker:
		port: 3306
```

```
readinessProbe:
	exec:
		command:
			- cat
			- /app/is_ready
```

Il est également possible de spécifier un temps d'attente avant de commencer à tester avec `initialDelaySeconds` et de préciser tout les combiens de temps effecter le test avec `periodSeconds`.

Si un [[Pod]] n'est pas prêt après trois essais, la Probe s'arrête. Cette limite par défaut est configurable avec `failureThreshold`.