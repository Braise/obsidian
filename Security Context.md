[Documentation k8s](https://kubernetes.io/docs/tasks/configure-pod-container/security-context/)

## Théorie

Définis les privilèges et les controles d'accès pour un [[Pod]] ou un container.
Cela peut prendre une des formes suivantes (mais n'est pas uniquement limité à ces dernières):

- Discretionary Access Control: Permission to access an object, like a file, is based on user ID (UID) and group ID (GID).
- Security Enhanced Linux (SELinux): Objects are assigned security labels.
- Running as privileged or unprivileged.
- Linux Capabilities: Give a process some privileges, but not all the privileges of the root user.
- AppArmor: Use program profiles to restrict the capabilities of individual programs.
- Seccomp: Filter a process's system calls.
- AllowPrivilegeEscalation: Controls whether a process can gain more privileges than its parent process. This bool directly controls whether the no_new_privs flag gets set on the container process. allowPrivilegeEscalation is always true when the container:
	- is run as privileged, or
	- has CAP_SYS_ADMIN

Pour inclure un Security Context dans un [[Pod]] il suffit d'ajouter le champs `securityContext` dans la spécification de ce dernier. La sécurité appliqué pour un [[Pod]] s'applique à tous les containers du [[Pod]]. Si la sécurité est appliquée au niveau du container, alors elle override celle du [[Pod]].
## Exemple

```
apiVersion: v1
kind: Pod
metadata:
  name: security-context-demo
spec:
  securityContext:
    runAsUser: 1000
    runAsGroup: 3000
    fsGroup: 2000
  volumes:
  - name: sec-ctx-vol
    emptyDir: {}
  containers:
  - name: sec-ctx-demo
    image: busybox:1.28
    command: [ "sh", "-c", "sleep 1h" ]
    volumeMounts:
    - name: sec-ctx-vol
      mountPath: /data/demo
    securityContext:
      allowPrivilegeEscalation: false
```

## Commandes