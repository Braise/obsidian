[Documentation k8s](https://kubernetes.io/docs/concepts/services-networking/service/)

## Théorie

Permet d'exposer une application roulant dans un ou plusieurs [[Pod]]s.

Quand un [[Pod]] est créé, une IP lui est assigné. Comme ils peut y avoir énormément de mouvement avec ceux-ci, c'est-à-dire qu'ils peuvent être détruit lors d'un scale down, puis de nouveaux peuvent être créés lors d'un scale up, même chose lors du déploiement d'une nouvelle version de l'application, il y a énormément d'adresse IP qui sont consommées. 
Cela illustre aussi que l'on ne devrait jamais accéder un [[Pod]] via son IP directement car on a aucune garantie qu'il est encore en vie.

Les Services pallient à cette problématique. Ils disposent de leur propre adresse IP, de leur propre DNS et de leur propre port. Et ces trois éléments sont *stables*.

Les services utilisent les [[Labels et Selecteurs]] pour identifier les [[Pod]]s auxquels envoyer le trafic.

Comme les Services agissent comme une sorte d'interface entre les [[Pod]]s de l'application et le reste du monde, ils permettent un loose coupling dans le cadre d'une architecture MicroService :

![[ServiceLooseCoupling.png]]

Un Service joue également le rôle de load balancer et va distribuer la charge parmis tous les [[Pod]]s dont il a la responsabilité.

Il existe trois type de Services.

### 1. ClusterIP

Le Service crée une adresse IP virtuelle *à l'intérieur du Cluster* afin de permettre la communication entre application. Par exemple pour que les serveurs front-end puissent communiquer avec les serveur back-end.

![[ClusterIP.png]]

Ci-dessus un exemple d'application divisée en trois parties où les [[Pod]]s utilisent des Services pour accéder les différentes parties.

Chaque Service créé se voit associé une ClusterIP qui est enregistrée, avec le nom du Service, dans le DNS interne du Cluster. Comme tous les [[Pod]]s sont préprogrammés pour utiliser le DNS du Cluster, cela signifie donc qu'ils peuvent convertir le nom du Service en sa ClusterIP.

Ce type de service est celui par défaut.

```
apiVersion: v1
kind: Service
metadata:
	name: hello-svc
spec:
	ports:
	- port: 8080
	selector:
		app: hello-world <<==== Send to Pods with these labels
		env: tkb <<==== Send to Pods with these labels
```

### 2. NodePort

Ce type de Service est construit par dessus le ClusterIP.

Un NodePort Service rend accessible une application contenue dans un [[Pod]] depuis l'extérieur du cluster en y forwardant le trafic externe :

![[NodePort.png]]

Quand un client effectue une requête sur un certain port du Cluster, le *NodePort*, le Service va transférer la requête vers le [[Pod]] adéquat.

Comme Il s'agit d'un "upgrade" d'un ClusterIP qui enregistre un DNS name, vitual IP et port dans le DNS du Cluster, un NodePort reprend ces trois éléments mais y ajoute également un port, le *NodePort*, qui est utilisé pour atteindre le Service depuis l'extérieur du Cluster.

```
apiVersion: v1
kind: Service
metadata:
	name: skippy
spec:
	type: NodePort
	ports:
		- port: 8080
		  nodePort: 30050
	selector:
		app: hello-world
```

Les [[Pod]]s dans le Cluster peuvent atteindre ce service par son nom, Skippy, sur le port 8080. Les clients externes au Cluster peuvent envoyer du trafic sur n'importe quelle node du Cluster, pour autant que cela soit sur le port 300500

### 3. LoadBalancer

Joue le rôle de load balancer pour une application en s'intégrant avec le load balancer sous-jacent de la plateforme Cloud utilisée. Ils ne fonctionnent que sur les Cloud qui les supportent.

## Commandes

`kubectl get service`
Liste les Services.