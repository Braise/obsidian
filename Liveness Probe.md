[Documentation k8s](https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/)

## Théorie

Chaque fois qu'une application crash, c'est-à-dire que le container dans le [[Pod]] s'arrête, Kubernetes tente de restart le container dans le [[Pod]] afin de restaurer le service aux clients. Cela fonctionne bien mais il existe un cas précis qu'il faut impérativement gérer : l'application crash mais le container reste en vie.

Dans ce cas précis, le container est up donc Kubernetes assume que l'application est up et qu'il n'y a rien à faire.

La Liveness Probe nous permet de configurer une Probe qui va périodiquement s'assurer que l'application dans le container est en vie. Si le test échoue, le container est considéré comme en erreur et est détruit et recréé.

La liveness se définit comme suit dans la définition d'un [[Pod]]:

```
apiVersion: v1
kind: Pod
metadata:
name: simple-webapp
labels:
	name: simple-webapp
spec:
	containers:
	- name: simple-webapp
	  image: simple-webapp
	  ports:
		  - containerPort: 8080
	  livenessProbe:
		  httpGet:
			  path: /api/healthy
			  port: 8080
```

Il est possible de configurer la Liveness Probe pour un appel http, comme au dessu, mais également pour un appel tcp ou une commande à exécuter:

```
livenessProbe:
	tcpSocker:
		port: 3306
```

```
livenessProbe:
	exec:
		command:
			- cat
			- /app/is_ready
```

Il est également possible de spécifier un temps d'attente avant de commencer à tester avec `initialDelaySeconds` et de préciser tout les combiens de temps effecter le test avec `periodSeconds`.

Si un [[Pod]] n'est pas heakthy après trois essais, la Probe s'arrête. Cette limite par défaut est configurable avec `failureThreshold`.