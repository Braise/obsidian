[Documentation k8s](https://kubernetes.io/docs/concepts/scheduling-eviction/taint-and-toleration/)

## Théorie

Il est possible de configurer une node pour empêcher certains [[Pod]] de rouler dessus. Et il est possible de faire l'inverse, de configurer une node pour attirer certains [[Pod]]. Ces deux concepts sont respectivement *Taints* et [[Node Affinity]].

*Toleration* est ce qui est appliqué à un [[Pod]] pour définir les *taints* auxquelles il est "immunisé".

Les *taints* et les *tolerations* fonctionnent donc de concert pour s'assurer que les [[Pod]] sont schedule sur les nodes appropriées.

Pour voir comment appliquer une *taint* sur une node, se référer à la section **Commandes** de la présente note.

Pour appliquer une *toleration* sur un [[Pod]], simplement ajouter la section suivante à la définition :

```
tolerations:
- key: "key1"
  operator: "Equal"
  value: "value1"
  effect: "NoSchedule"
```

l'`operator` a comme valeur par défaut `Equal`. Une *toleration* est un match pour une *taint* si les clés sont les mêmes, que les effets sont identiques et :
- Que l'`operator` est `Exists`, dans ce cas aucune `value` ne doit être précisée
- Que l'`operator` est `Equal` et que les `value` sont égales

Toutefois, il y a deux exceptions importantes :

1. Si la clé est vide et que l'`operator` utilisé est `Exists` alors cela match toutes les clés, valeurs et effets. Cela tolère donc tout.
2. Si la section `effect` est vide, alors cela match toute les clés identiques. Dans l'exemple ci-dessus cela matcherais toutes les `key1`.

Les valeurs possibles pour `Effect` sont :
- `NoSchedule`
- `PreferNoSchedule` : Version plus douce de la précédente. Le system va essayer de ne pas placer de [[Pod]] qui ne sont pas tolérants mais ce n'est pas un prérequis
- `NoExecute` : Affecte uniquement les [[Pod]] s'exécutant déjà sur une node comme suit :
	- Ceux qui ne tolèrent pas la *taint* sont éjectés immédiatement
	- Ceux qui tolèrent la *taint* sans spécifier de temps, via `tolerationSeconds`, restent indéfiniment
	- Ceux qui tolèrent la *taint* en spécifiant un laps de temps restent pour la durée définie

## Commandes

`kubectl taint nodes <nom> <key>=<value>:<effect>`
Place une *taint* sur une node s'appliquant sur un effet. Par exemple `kubectl taint node node1 key1=value1:NoSchedule` applique une *taint* sur la `node1`. Cette *taint* à la clé `key1` et la valeur `value1` et affecte `NoSchedule`.
En d'autre termes, aucun [[Pod]] ne pourra être schedule sur la `node1` à moins d'avoir une *toleration* correspondante.

`kubectl taint nodes node1 <key>=<value>:NoSchedule-`
Retire une *taint* d'une node.