[Documentation k8s](https://kubernetes.io/docs/concepts/security/service-accounts/)

## Théorie
Type de compte utilisés par des machines. Ils peuvent être utilisés par des applications pour intéragir avec un cluster. Par exemple Prometheus utilise un Service Account pour obtenir les informations du cluster.

Quand on crée un Service Account, cela crée automatiquement un *token*. Celui-ci est utilisé par l'utilisateur du compte pour s'authentifier auprès de l'API k8s. 
Ce *token* est stocké comme un [[Secret]].

Il peut survenir le cas où l'application devant accéder le cluster, et donc utiliser un *token* d'un Service Account,  est hébergé sur le cluster lui-même. Dans ce cas, il est possible de monter le *token* comme un volume dans le [[Pod]].

Pour chaque namespace un Service Account, *default*, est automatiquement créé Quand un [[Pod]] est créé. ce compte par défaut et son [[Secret]] sont automatiquement montés dedans. Ce compte dispose de permissions restreintes donnant accès uniquement aux commandes basique de l'API.
Il est toutefois possible de spécifier un autre Service Account avec le champs **serviceAccountName** et en y indiquant le nom du Service Account à utiliser. 

Il est également possible d'empêcher K8S de mount le Service Account par défaut en utilisant le champs **automountServiceAccountToken** avec le valuer *false*.

Depuis la version 1.22, il est maintenant recommandé d'obtenir un token depuis l'application en utilisant la [TokenRequestApi]([TokenRequest | Kubernetes](https://kubernetes.io/docs/reference/kubernetes-api/authentication-resources/token-request-v1/)).

## Commandes

`kubectl create serviceaccount <name>`
Crée un Service Account.

`kubectl get serviceaccount`
Liste les Service Account.

`kubectl describe serviceaccount <name>`
Obtient les informations détaillées d'un Service Account.

`kubectl create token <Service Account name>`
Crée un token pour un Service Account.