[Documentation k8s](https://kubernetes.io/docs/concepts/workloads/controllers/replicaset/)

## Théorie

Un ReplicaSet s'assure que le nombre de [[Pod]] désirés est toujours up.  Un [[Deployment]] utilise un ReplicaSet pour manager les [[Pod]]. 
Il est recommandé de ne pas manager les ReplicaSets directement et de laisser les [[Deployment]] s'en occuper.
Pour comprendre la relation entre des [[Pod]], ReplicaSet et [[Deployment]], on peut résumer ce;a de la facon suivante : 

> Des [[Deployment]] gèrent des ReplicaSet qui gèrent des [[Pod]]

Un ReplicaSet possède les caractéristiques suivantes :

- Un selector : permet d'identifier les [[Pod]] géré par le ReplicaSet
- Replicas : Un nombre spécifiant le nombre de [[Pod]] à maintenir
- Un [[Pod]] template : Description des [[Pod]] 

Sur base de ces informations, un ReplicaSet va lever où détruire des [[Pod]] afin de maintenir le nombre correct de replica. Les [[Pod]] sont créés sur base du template fournit dans le manifeste.

## Exemple

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: frontend
  labels:
    app: guestbook
	tier: frontend
spec:
  # modify replicas according to your case
  replicas: 3
  selector: # Ici le selector qui permet au ReplicaSet d'identifier les Pods à manager
    matchLabels:
      tier: frontend
  template:
    metadata:
      labels:
        tier: frontend # Le label ici correspond au selector du ReplicaSet
    spec:
      containers:
      - name: php-redis
        image: gcr.io/google_samples/gb-frontend:v3
```

Manifest d'un ReplicaSet créant 3 [[Pod]] avec l'image `gcr.io/google_samples/gb-frontend:v3`

![[Structure_replicaset.png]]

## Commandes

`kubectl get replicaset`
Liste les ReplicaSets.

`kubectl describe replicaset new-replica-set`
Décrit un ReplicaSet.

`kubectl delete replicaset <name>`
Delete un ReplicaSet.

`kubectl edit replicaset <name>`
Edit un ReplicaSet.
Après avoir fini et appliqué les changements, il faut delete les [[Pod]] existants pour que de nouveaux puissent être levés avec les changements.

`kubectl scale replicaset --replicas=<nombre> <name>`
Ajuste le nombre de [[Pod]] du ReplicaSet.