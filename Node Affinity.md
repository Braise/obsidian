[Documentation k8s](https://kubernetes.io/docs/concepts/scheduling-eviction/assign-pod-node/#node-affinity)

## Théorie
Contrairement aux [[Taints and Tolerations]], le concept de Node Affinity est utilisé pour définir au niveau du [[Pod]] sur quelle(s) node(s) on veut le faire rouler. L'approche recommandé pour mettre en place cela est via les `label selector`.

Comme énormément d'objet dans K8S, les nodes peuvent avoir des `labels`. Il est donc possible d'ajouter un `nodeSelector` à un [[Pod]] et de lui spécifier via ce selector quels labels utiliser pour choisir une Node. Ici un exemple d'un [[Pod]] utilisant le selector :

```
apiVersion: v1
kind: Pod
metadata:
  name: nginx
  labels:
    env: test
spec:
  containers:
  - name: nginx
    image: nginx
    imagePullPolicy: IfNotPresent
  nodeSelector:
    disktype: ssd
```

Il est possible de préciser si la règle est `soft` ou `preferred`.

Il est également possible d'utiliser un `nodeSelector` si l'on veut une granularité plus fine des règles :

```
apiVersion: v1
kind: Pod
metadata:
  name: with-node-affinity
spec:
  affinity:
    nodeAffinity:
      requiredDuringSchedulingIgnoredDuringExecution:
        nodeSelectorTerms:
        - matchExpressions:
          - key: topology.kubernetes.io/zone
            operator: In
            values:
            - antarctica-east1
            - antarctica-west1
      preferredDuringSchedulingIgnoredDuringExecution:
      - weight: 1
        preference:
          matchExpressions:
          - key: another-node-label-key
            operator: In
            values:
            - another-node-label-value
  containers:
  - name: with-node-affinity
    image: registry.k8s.io/pause:2.0
```

L'exemple ci-dessus fais usage des deux types de Node Affinity:
- `requiredDuringSchedulingIgnoredDuringExecution` : Si le Scheduler ne parvient pas à trouver une Node qui satisfait la règle alors le [[Pod]] n'est pas déployé
- `preferredDuringSchedulingIgnoredDuringExecution`: Le Scheduler tente de trouver une node satisfaisant la règle. Si il n'y parvient pas le [[Pod]] est quand même déployé.

Il est possible d'utiliser les `operator` suivant avec le `nodeAffinity` :

| Operator | Comportement |
|----------|--------------|
| In | La valeur du label est présente dans la liste|
| NotIn | La valeur du label n'est pas présente dans la liste|
| Exists | Un label avec la clé existe sur l'objet |
| DoesNotExists | Un label avec la clé n'existe pas sur l'objet |

## Commandes

`kubectl label nodes <node name> <key>=<valeur>`
Ajoute un label à une Node